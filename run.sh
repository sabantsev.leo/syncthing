#!/bin/bash -l
if [ -z "$1" ]; then
    echo 'arg <project name>'
    exit 1
fi

apt update && apt install -y ntp mc htop screen wget containerd docker.io docker docker-compose dialog apt-utils \
                  git curl python3-dev python3-pip syncthing

pip3 install lxml
systemctl enable docker
service ntp restart

docker-compose down
docker-compose kill
docker volume prune



if [ ! -d "FirstConfig" ]; then
  mkdir "FirstConfig"
fi

if [ ! -d "SecondConfig" ]; then
  mkdir "SecondConfig"
fi

if [ ! -d "First" ]; then
  mkdir "First"
fi

if [ ! -d "Second" ]; then
  mkdir "Second"
fi

chmod -R 777 ./FirstConfig
chmod -R 777 ./SecondConfig
chmod -R 777 ./First
chmod -R 777 ./Second

syncthing -generate="./FirstConfig"
syncthing -generate="./SecondConfig"

touch .env
python3 config_parse.py "./FirstConfig/config.xml" "1" > .env
python3 config_parse.py "./SecondConfig/config.xml" "2" >> .env

cat .env

cp "config_init.py " "./FirstConfig/"
cp "config_init.py " "./SecondConfig/"

docker-compose up -d --build --force-recreate --remove-orphans