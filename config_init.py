from lxml import etree
from lxml import objectify
import sys


class Device:
    """Wrapper for devices"""

    device = objectify.Element("device")
    device_id = ""
    device_name = ""

    def __init__(self, device_id, device_name):
        self.device = objectify.Element("device")
        self.device_id = device_id
        self.device_name = device_name

        self.device.set("id", device_id)
        self.device.set("name", device_name)
        self.device.set("compression", "metadata")
        self.device.set("introducer", "false")
        self.device.set("skipIntroductionRemovals", "false")
        self.device.set("introducedBy", "")

        self.device.address = "dynamic"
        self.device.paused = "false"
        self.device.autoAcceptFolders = "false"
        self.device.maxSendKbps = "0"
        self.device.maxRecvKbps = "0"
        self.device.maxRequestKiB = "0"
        self.device.untrusted = "0"
        self.device.remoteGUIPort = "0"

    def get(self):
        return self.device


class Folder:
    """Wrapper for folders"""

    folder = objectify.Element("folder")

    def __init__(self, folder_id, devices):
        self.folder.set("id", folder_id)
        self.folder.set("label", "Public Folder")
        self.folder.set("path", "/var/syncthing/Sync")
        self.folder.set("type", "sendreceive")
        self.folder.set("rescanIntervalS", "3600")
        self.folder.set("fsWatcherEnabled", "true")
        self.folder.set("fsWatcherDelayS", "10")
        self.folder.set("ignorePerms", "false")
        self.folder.set("autoNormalize", "true")

        self.folder.filesystemType = "basic"

        for device_object in devices:
            device_description = objectify.Element("device")
            device_description.set("id", device_object.device_id)
            device_description.set("introducedBy", "")
            device_description.encryptionPassword = ""
            self.folder.append(device_description)

        self.folder.minDiskFree = "1"
        self.folder.minDiskFree.set("unit", "%")

        versioning = objectify.Element("versioning")
        versioning.cleanupIntervalS = "3600"
        versioning.fsPath = ""
        versioning.fsType = "basic"
        self.folder.append(versioning)

        self.folder.copiers = "0"
        self.folder.pullerMaxPendingKiB = "0"
        self.folder.hashers = "0"
        self.folder.order = "random"
        self.folder.ignoreDelete = "false"
        self.folder.scanProgressIntervalS = "0"
        self.folder.pullerPauseS = "0"
        self.folder.maxConflicts = "10"
        self.folder.disableSparseFiles = "false"
        self.folder.disableTempIndexes = "false"
        self.folder.weakHashThresholdPct = "25"
        self.folder.markerName = ".stfolder"
        self.folder.copyOwnershipFromParent = "false"
        self.folder.modTimeWindowS = "0"
        self.folder.maxConcurrentWrites = "2"
        self.folder.disableFsync = "false"
        self.folder.blockPullOrder = "standart"
        self.folder.copyRangeMethod = "standart"
        self.folder.caseSensitiveFS = "false"
        self.folder.junctionsAsDirs = "false"
        self.folder.syncOwnership = "false"
        self.folder.scanOwnership = "false"

    def get(self):
        return self.folder


class Configuration:
    """Wrapper for configuration"""

    configuration = objectify.Element("configuration")
    configuration.set("version", "36")

    def __init__(self, folders, devices, apikey):
        for folder_object in folders:
            self.configuration.append(folder_object.get())

        for device_object in devices:
            self.configuration.append(device_object.get())

        gui = objectify.Element("gui")
        gui.set("enabled", "true")
        gui.set("tls", "false")
        gui.set("debugging", "false")
        gui.address = "127.0.0.1:8384"
        gui.apikey = apikey
        gui.theme = "default"
        self.configuration.append(gui)

        self.configuration.append(objectify.Element("ldap"))

        options = objectify.Element("options")
        options.listenAddress = "default"
        options.globalAnnounceServer = "default"
        options.globalAnnounceEnabled = "false"
        options.localAnnounceEnabled = "true"
        options.localAnnouncePort = "21027"
        options.localAnnounceMCAddr = "[ff12::8384]:21027"
        options.maxSendKbps = "0"
        options.maxRecvKbps = "0"
        options.reconnectionIntervalS = "60"
        options.relaysEnabled = "true"
        options.relayReconnectIntervalM = "10"
        options.startBrowser = "false"
        options.natEnabled = "true"
        options.natLeaseMinutes = "60"
        options.natRenewalMinutes = "30"
        options.natTimeoutSeconds = "10"
        options.urAccepted = "-1"
        options.urSeen = "3"
        options.append(objectify.Element("urUniqueID"))
        options.urURL = "https://data.syncthing.net/newdata"
        options.urPostInsecurely = "false"
        options.urInitialDelayS = "1000"
        options.autoUpgradeIntervalH = "12"
        options.upgradeToPreReleases = "false"
        options.keepTemporariesH = "24"
        options.cacheIgnoredFiles = "false"
        options.progressUpdateIntervalS = "5"
        options.limitBandwidthInLan = "false"

        options.minHomeDiskFree = "1"
        options.minHomeDiskFree.set("unit", "%")

        options.releasesURL = "https://upgrades.syncthing.net/meta.json"
        options.overwriteRemoteDeviceNamesOnConnect = "false"
        options.tempIndexMinBlocks = "10"
        options.unackedNotificationID = "authenticationUserAndPassword"
        options.trafficClass = "0"
        options.setLowPriority = "true"
        options.maxFolderConcurrency = "0"
        options.crashReportingURL = "https://crash.syncthing.net/newcrash"
        options.crashReportingEnabled = "true"
        options.stunKeepaliveStartS = "180"
        options.stunKeepaliveMinS = "20"
        options.stunServer = "default"
        options.databaseTuning = "auto"
        options.maxConcurrentIncomingRequestKiB = "0"
        options.announceLANAddresses = "true"
        options.sendFullIndexOnUpgrade = "false"
        options.connectionLimitEnough = "0"
        options.connectionLimitMax = "0"
        options.insecureAllowOldTLSVersions = "false"

        self.configuration.append(options)
        self.configuration.append(objectify.Element("defaults"))

    def get(self):
        return self.configuration


args = sys.argv
node_id = args[1]
target_node_id = args[2]
folder_id = args[3]
device1 = Device(node_id, "my-device1")
device2 = Device(target_node_id, "my-device2")
folder = Folder(folder_id, [device1, device2])
configuration = Configuration([folder, ], [device1, device2], "oqxzQK9KQPMxb3KpTqQXKepxqkXeWFZf")

result = configuration.get()
objectify.deannotate(result)
etree.cleanup_namespaces(result)

try:
    with open("config.xml", "wb") as xml_writer:
        xml_writer.write(etree.tostring(result, pretty_print=True))
except IOError:
    pass


