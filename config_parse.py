from lxml import objectify
import sys


args = sys.argv
path = args[1]

with open(path) as f:
    xml = f.read()

root = objectify.fromstring(xml)
attr = root.device.attrib

print("NODE_ID" + args[2] + "=" + attr['id'])
