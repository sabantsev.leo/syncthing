FROM alpine:3.16

ARG uid=1000
ARG gid=100
ARG NODE_ID
ARG TARGET_NODE_ID
ARG FOLDER_ID
ARG CONFIG_FOLDER_NAME

ENV PYTHONUNBUFFERED=1



RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip lxml

RUN apk --no-cache add ca-certificates curl && \
    tag=$(curl -sX GET "https://api.github.com/repos/syncthing/syncthing/releases/latest" \
        | awk '/tag_name/{print $4;exit}' FS='[""]') && \
    curl -o \
        /tmp/syncthing.tar.gz -L \
        https://github.com/syncthing/syncthing/releases/download/$tag/syncthing-linux-amd64-$tag.tar.gz && \
    tar xf /tmp/syncthing.tar.gz -C /tmp/ && \
    mv /tmp/sync*/syncthing /usr/local/bin/syncthing && \
    # Change `users` gid to match the passed in $gid
    [ $(getent group users | cut -d: -f3) == $gid ] || \
            sed -i "s/users:x:[0-9]\+:/users:x:$gid:/" /etc/group && \
    adduser -h /home/syncthing -DG users -u $uid syncthing && \
    mkdir /config

#VOLUME ./$CONFIG_FOLDER_NAME/config /config

RUN chown -R syncthing:users /config && \
    apk del curl && \
    rm -rf /tmp/*

COPY ./$CONFIG_FOLDER_NAME/* /config/
COPY config_init.py /config/config_init.py

RUN cd /config && python3 config_init.py $NODE_ID $TARGET_NODE_ID $FOLDER_ID

USER syncthing

ENTRYPOINT ["syncthing"]
CMD ["-home=/config", "-gui-address=0.0.0.0:8384"]
